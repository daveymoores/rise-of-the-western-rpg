$(document).foundation();


$(document).ready(function(){

	if($('#no_age_cookie').length) {
		$('#no_age_cookie').appendTo('#start_modal').css('visibility', 'visible');
		$('#start_modal').foundation('reveal', 'open');
		$('.chosen-container').css('width', '31.5%');
	}


   new Share(".share_btn", {
		ui: {
		  flyout: "bottom center"
		},
		networks: {
		    google_plus: {
		      enabled: true,
		      url:     "uk-microsites.ign.com/thewitcherfilmclub/"
		    },
		    twitter: {
		      enabled: true,
		      url:     "uk-microsites.ign.com/thewitcherfilmclub/",
		      description:    "Vote for your favourite monster and hunting movie so @IGNUK and @witchergame can put on a special screenings for the winning films!"
		    },
		    facebook: {
		      enabled: true,
		      load_sdk: true,// Load the FB SDK. If false, it will default to Facebook's sharer.php implementation.
		                // NOTE: This will disable the ability to dynamically set values and rely directly on applicable Open Graph tags.
		                // [Default: true]
		      url: "uk-microsites.ign.com/thewitcherfilmclub/",
		      //app_id: "551643344962507",
		      title: "Vote for your favourite monster and hunting movie with IGN and The Witcher 3",
		      caption: "Vote for your favourite monster and hunting movie with IGN and The Witcher 3",
		      description:    "Vote for your favourite monster and hunting movie so @IGNUK and @witchergame can put on a special screenings for the winning films!"
		      //image: // image to be shared to Facebook [Default: config.image]
		    },
		    pinterest: {
		      enabled: true,
		      url:     "uk-microsites.ign.com/thewitcherfilmclub/",
		      //image:   // image to be shared to Pinterest [Default: config.image]
		      description:    "Vote for your favourite monster and hunting movie so @IGNUK and @witchergame can put on a special screenings for the winning films!"
		    },
		    email: {
		      enabled: true,
		      title:     "Vote for your favourite monster and hunting movie with IGN and The Witcher 3",
		      description:    "Vote for your favourite monster and hunting movie so @IGNUK and @witchergame can put on a special screenings for the winning films!"
		    }
		  }

   });



	function bounceBtn(){
    	if($('.scrollBtn').hasClass('bounce')) {
    		$('.scrollBtn').removeClass('bounce');
    		setTimeout(bounceBtn, 3000);
    	} else {
    		$('.scrollBtn').addClass('bounce');
    		setTimeout(bounceBtn, 3000);
    	}

    }

    setTimeout(bounceBtn, 5000);



      $(window).resize(function() {
          $('#hero').height($(window).height());
          //$('#static > video').height($(window).height());
          $('#block').height($(window).height());
          $('#block_II').height($(window).height());
          $('#block_III').height($(window).height());
          $('#block_IV').height($(window).height());
          $('#block_V').height($(window).height());
          $('#block_VI').height($(window).height());
          $('#block_VII').height($(window).height());
          $('#block_VIII').height($(window).height());
      });


      /**window slide behaviour**/

      $(window).resize(function() {

      	var winWidth = $(window).width();

			console.log(winWidth);

      	$('#primary-content').css('width', winWidth);
      	$('#static > video').css('width', winWidth);
      	$('#subpage-right').css('width', winWidth);
      	$('.multi-img-bg').find('video').css('width', winWidth/2);
      	$('#block').css('width', winWidth);
			$('#block_II').css('width', winWidth);


      });


      $(window).trigger('resize');


});






// $(document).ready(function(){
//
// 	// Cache the Window object
// 	$window = $(window);
//
// 	$('section[data-type="background"]').each(function(){
//      var $bgobj = $(this); // assigning the object
//
//       $(window).scroll(function() {
//
// 			// Scroll the background at var speed
// 			// the yPos is a negative value because we're scrolling it UP!
// 			var yPos = -($window.scrollTop() / $bgobj.data('speed'));
//
// 			// Put together our final background position
// 			var coords = '50% '+ yPos + 'px';
//
// 			// Move the background
// 			$bgobj.css({ backgroundPosition: coords });
//
// 		}); // window scroll Ends
// 	 });
//
// });
/*
 * Create HTML5 elements for IE's sake
 */

document.createElement("article");
document.createElement("section");

$(document).ready(function(){

	// var $horizontal = $('#block').find('img');
	//
	//
   //  $(window).scroll(function () {
   //      var s = $(this).scrollTop(),
   //          d = $(document).height(),
   //          c = $(this).height();
	//
	// 		var secWidth = $('section').width();
	// 		var blokWidth = $horizontal.width();
	//
	//
   //      scrollPercent = (s / (d - c))*100;
	//
   //      var position = Math.round((scrollPercent * (secWidth-10 - blokWidth)));
	//
	//        	$horizontal.css({
	//             'left': (position*3),
	//             'top': (position*6)
	//         });
	//
   //  });


	//first article - trigger when in view and change background to position fixed
	// $('#block').waypoint(function(direction){
	//
	// 	var $ele = $("#associated-content");
	//
	// 	if(direction == 'down') {
	//  		 $(this).addClass('stuck');
	//
	//  		 //animate associated content
	//  		 $ele.parent().transition({
	// 				marginBottom : '100px'
	// 			}, 300, 'ease', function(){
	//
	// 				// $ele.find('.content-sgl').each(function(i){
	//
	// 				// 	$(this).transition({
	// 				// 		y : -90,
	// 				// 		delay : (i*100)
	// 				// 	}, 500, 'ease');
	//
	// 				// });
	//
	// 			}).addClass('open');
	//
	// 	} else if(direction == 'up') {
	// 		$(this).removeClass('stuck');
	//
	// 		//animate associated content
	//
	//  		 $ele.parent().transition({
	// 				marginBottom : '0px'
	// 			}, 300, 'ease', function(){
	//
	// 				// $ele.find('.content-sgl').each(function(i){
	//
	// 				// 	$(this).transition({
	// 				// 		y : 0,
	// 				// 		delay : (i*100)
	// 				// 	}, 500, 'ease');
	//
	// 				// });
	//
	// 			}).addClass('open');
	// 	}
	// });
	//
	// $('#block').waypoint(function(direction){
	// 	if(direction == 'down') {
	//  		 $(this).addClass('stuck');
	// 	} else if(direction == 'up') {
	// 		$(this).removeClass('stuck');
	// 	}
	// });

	//Third article - trigger when in view and change background to position fixed



	//blok 2
	//second article- trigger when in view and change background to position fixed
	// $('#block_II').waypoint(function(direction){
	// 	if(direction == 'down') {
	//  		 $(this).addClass('stuck');
	// 	} else if(direction == 'up') {
	// 		$(this).removeClass('stuck');
	// 	}
	// });
	//
	// $('#block_II').waypoint(function(direction){
	// 	if(direction == 'down') {
	//
	// 	} else if(direction == 'up') {
	//
	// 	}
	// }, { offset: '70%' });

	$('#midvid-cont1').waypoint(function(direction) {

		var $quote = $('.quote');

		$(window).scroll(function () {
	        var s = $(this).scrollTop(),
	            d = $(document).height(),
	            c = $(this).height();

				var secWidth = $('section').width();
				var blokWidth = $quote.width();


	        scrollPercent = (s / (d - c))*100;

	        var position = Math.round(((scrollPercent*6) * (secWidth-10 - blokWidth)));



		       	$quote.css({
		            'left': (position*2)
		        });

	    });

		if(direction == 'down') {
			//$('#block_II').removeClass('stuck').addClass('stuckBtm');
			$('#block').hide();
			$('#first-mid-vid').removeClass('hidden');
		} else {
			//$('#block_II').addClass('stuck').removeClass('stuckBtm');
			$('#block').show();
			$('#first-mid-vid').addClass('hidden').prev().removeClass('hidden');
		}
	}, { offset: '100%' });

	//End blok 2

	//Blok 4
	// $('#block_III').waypoint(function(direction){
	//
	// 	if(direction == 'down') {
	//  		 $(this).addClass('stuck');
	// 	} else if(direction == 'up') {
	// 		$(this).removeClass('stuck');
	// 	}
	// });
	//
	// $('#block_III').waypoint(function(direction){
	// 	if(direction == 'down') {
	//
	// 	} else if(direction == 'up') {
	//
	// 	}
	// }, { offset: '70%' });


	// $('#block_IV').waypoint(function(direction) {
	// 	if(direction == 'down') {
	// 		$('#block_III').removeClass('stuck').addClass('stuckBtm');
	// 	} else {
	// 		$('#block_III').addClass('stuck').removeClass('stuckBtm');
	// 	}
	// }, { offset: '100%' });
	// //end blok 4
	//
	//
	//
	// //Blok 4
	// $('#block_IV').waypoint(function(direction){
	// 	if(direction == 'down') {
	//  		 $(this).addClass('stuck');
	// 	} else if(direction == 'up') {
	// 		$(this).removeClass('stuck');
	// 	}
	// });

	// $('#block_IV').waypoint(function(direction){
	// 	if(direction == 'down') {
	//
	// 	} else if(direction == 'up') {
	//
	// 	}
	// }, { offset: '70%' });

	$('#midvid-cont2').waypoint(function(direction) {
		if(direction == 'down') {
			// $('#block_IV').removeClass('stuck').addClass('stuckBtm');
			$('#second-mid-vid').removeClass('hidden').prev().addClass('hidden');
		} else {
			// $('#block_IV').addClass('stuck').removeClass('stuckBtm');
			$('#second-mid-vid').addClass('hidden').prev().removeClass('hidden');
		}
	}, { offset: '100%' });
	//end blok 4


	//Blok 5
	$('#block_V').waypoint(function(direction){
		if(direction == 'down') {
	 		 $(this).addClass('stuck');
		} else if(direction == 'up') {
			$(this).removeClass('stuck');
		}
	});

	$('#block_V').waypoint(function(direction){
		if(direction == 'down') {

		} else if(direction == 'up') {

		}
	}, { offset: '70%' });

	$('#block_VI').waypoint(function(direction) {
		if(direction == 'down') {
			$('#block_V').removeClass('stuck').addClass('stuckBtm');
		} else {
			$('#block_V').addClass('stuck').removeClass('stuckBtm');
		}
	}, { offset: '100%' });
	//End blok 5

	//end Blok 6
	$('#block_VI').waypoint(function(direction){
		if(direction == 'down') {
	 		 $(this).addClass('stuck');
		} else if(direction == 'up') {
			$(this).removeClass('stuck');
		}
	});

	$('#block_VI').waypoint(function(direction){
		if(direction == 'down') {

		} else if(direction == 'up') {

		}
	}, { offset: '70%' });

	$('#midvid-cont3').waypoint(function(direction) {
		if(direction == 'down') {
			$('#block_VI').removeClass('stuck').addClass('stuckBtm');
			$('#third-mid-vid').removeClass('hidden').prev().addClass('hidden');
		} else {
			$('#block_VI').addClass('stuck').removeClass('stuckBtm');
			$('#third-mid-vid').addClass('hidden').prev().removeClass('hidden');
		}
	}, { offset: '100%' });
	//End blok 6


	//Blok 7
	$('#block_VII').waypoint(function(direction){
		if(direction == 'down') {
	 		 $(this).addClass('stuck');
		} else if(direction == 'up') {
			$(this).removeClass('stuck');
		}
	});

	$('#block_VII').waypoint(function(direction){
		if(direction == 'down') {

		} else if(direction == 'up') {

		}
	}, { offset: '50%' });


	$('#block_VIII').waypoint(function(direction) {
		if(direction == 'down') {
			$('#block_VII').removeClass('stuck').addClass('stuckBtm');
		} else {
			$('#block_VII').addClass('stuck').removeClass('stuckBtm');
		}
	}, { offset: '100%' });
	//End Blok 7



	//Blok 7
	$('#block_VIII').waypoint(function(direction){
		if(direction == 'down') {
	 		 $(this).addClass('stuck');
	 		 $('#fourth-mid-vid').removeClass('hidden').prev().addClass('hidden');
		} else if(direction == 'up') {
			$(this).removeClass('stuck');
			$('#fourth-mid-vid').addClass('hidden').prev().removeClass('hidden');
		}
	});

	$('#block_VIII').waypoint(function(direction){
		if(direction == 'down') {

		} else if(direction == 'up') {

		}
	}, { offset: '50%' });


	$('#block_VIII').waypoint(function(direction){
		if(direction == 'down') {

			 $('#associated-content').parent().transition({
				marginBottom : '0px'
			}, 300, 'ease');


		} else if(direction == 'up') {

			$('#associated-content').parent().transition({
				marginBottom : '100px'
			}, 300, 'ease');

		}
	}, { offset: '-95%' });


	$('#midvid-cont4').waypoint(function(direction) {
		if(direction == 'down') {
			$('#block_VIII').removeClass('stuck').addClass('stuckBtm');
		} else {
			$('#block_VIII').addClass('stuck').removeClass('stuckBtm');
		}
	}, { offset: '100%' });
	//End Blok 7




	/**changing video on the fly**/
	$('#halflife').waypoint(function(direction){
		if(direction == 'down') {
	 		 $('#block_II').find('video').eq(0).fadeOut(200, function(){
	 		 		$('#block_II').find('#halflifeVid').fadeIn('fast');
	 		 });

		} else if(direction == 'up') {

	 		 $('#block_II').find('#halflifeVid').fadeOut(200, function(){
	 		 	$('#block_II').find('video').eq(0).fadeIn('fast');
	 		 });

		}
	});



	/**changing video on the fly**/
	// $('#unreal').waypoint(function(direction){
	// 	if(direction == 'down') {
	//  		 $('#block_III').find('video').eq(0).fadeOut(200, function(){
	//  		 		$('#block_III').find('#unrealVid').fadeIn(200);
	//  		 });
	//
	// 	} else if(direction == 'up') {
	//
	//  		 $('#block_III').find('#unrealVid').fadeOut(200, function(){
	//  		 	$('#block_III').find('video').eq(0).fadeIn(200);
	//  		 });
	//  		// $('#block_III').find('#unrealVid').get(0).pause();
	// 	}
	// });

		/**changing video on the fly**/
	// $('#allied').waypoint(function(direction){
	// 	if(direction == 'down') {
	//  		 $('#block_IV').find('video').eq(0).fadeOut(200, function(){
	//  		 		$('#block_IV').find('#alliedVid').fadeIn(200);
	//  		 });
	//
	// 	} else if(direction == 'up') {
	//
	//  		 $('#block_IV').find('#alliedVid').fadeOut(200, function(){
	//  		 	$('#block_IV').find('video').eq(0).fadeIn(200);
	//  		 });
	//  		// $('#block_IV').find('#alliedVid').get(0).pause();
	// 	}
	// });

			/**changing video on the fly**/
	$('#farcry').waypoint(function(direction){
		if(direction == 'down') {
	 		 $('#block_V').find('video').eq(0).fadeOut(200, function(){
	 		 		$('#block_V').find('#farcryVid').fadeIn(200);
			});

		} else if(direction == 'up') {

	 		 $('#block_V').find('#farcryVid').fadeOut(200, function(){
	 		 	$('#block_V').find('video').eq(0).fadeIn(200);
	 		 });
	 		// $('#block_V').find('#farcryVid').get(0).pause();
		}
	});

	/**changing video on the fly**/
	$('#bioshock').waypoint(function(direction){
		if(direction == 'down') {
	 		 $('#block_VI').find('video').eq(0).fadeOut(200, function(){
	 		 		$('#block_VI').find('#bioshockVid').fadeIn(200);
	 		 });

		} else if(direction == 'up') {

	 		 $('#block_VI').find('#bioshockVid').fadeOut(200, function(){
	 		 	$('#block_VI').find('video').eq(0).fadeIn(200);
	 		 });
	 		// $('#block_VI').find('#bioshockVid').get(0).pause();
		}
	});



	/**changing video on the fly**/
	$('#borderlands').waypoint(function(direction){
		if(direction == 'down') {
	 		 $('#block_VII').find('video').eq(0).fadeOut(200, function(){
	 		 		$('#block_VII').find('#borderlandsVid').fadeIn(200);
	 		 });

		} else if(direction == 'up') {

	 		 $('#block_VII').find('#borderlandsVid').fadeOut(200, function(){
	 		 	$('#block_VII').find('video').eq(0).fadeIn(200);
	 		 });
	 		//$('#block_VII').find('#borderlandsVid').get(0).pause();
		}
	});


	/**changing video on the fly**/
	$('#titan').waypoint(function(direction){
		if(direction == 'down') {
	 		 $('#block_VIII').find('video').eq(0).fadeOut(200, function(){
	 		 		$('#block_VIII').find('#titanVid').fadeIn(200);
	 		 });

		} else if(direction == 'up') {

	 		 $('#block_VIII').find('#titanVid').fadeOut(200, function(){
	 		 	$('#block_VIII').find('video').eq(0).fadeIn(200);
	 		 });
	 		// $('#block_VIII').find('#titanVid').get(0).pause();
		}
	});








	/**image change**/

	function dunGoneChanged(whichWay, el){
		if(whichWay == 'down') {
	 		if(el.find('span.active').next().length == 0) {

	 		} else {
	 			el.find('span.active').removeClass('active').next().addClass('active');
	 		}
		} else if(whichWay == 'up') {
			if(el.find('span.active').prev().length == 0) {

	 		} else {
				el.find('span.active').removeClass('active').prev().addClass('active');
			}
		}
	}

	for(i=0; i < 21; i++) {

		$('#block_II').waypoint(function(direction) {

			var $this = $(this);
			dunGoneChanged(direction, $this);

		}, { offset: (-i*60) + 'px' });

	}





	$('.triggerNu').waypoint(function(direction) {

		if(direction == 'down') {
			$('#block_II').removeClass('stuck').addClass('stuckBtm');
		} else {
			$('#block_II').addClass('stuck').removeClass('stuckBtm');
		}

	}, { offset: '100%' });


	$.stellar();
});


$(window).scroll(function() {
	// The social div
	var $socialDiv = $('.full-width-bg'),
		$holdDiv = $('#first-mid-vid'),
		windowScroll = $(this).scrollTop(),
	  	elementOffset = $holdDiv.offset().top,
    	distance      = (elementOffset - windowScroll);


	// console.log();

	//Slow scroll of social div and fade it out
	$socialDiv.css({
	//'margin-top' : - (windowScroll / 3) + "px",
	'opacity' : 1 - (windowScroll / 550)
	});

	// console.log('divscroll =' + elementOffset);

	// $holdDiv.css({
	// 	'opacity' : 1 - (windowScroll / 7000)
	// });
});




// $(document).ready(function(){
// 	$('.article').waypoint(function(direction) {
//
// 		if($(this).hasClass('middle-vid') !== true) {
//
// 			var $this = $(this);
// 			var $ele = $("#associated-content");
//
// 			var x = $this.attr('data-num');
// 			var y = $this.prev().attr('data-num');
// 			console.log('data number ' + x);
//
// 			if(direction == 'down') {
//
//
// 				if(x=='2') {
//
// 					$ele.find('.content-sgl').each(function(i){
//
// 						switchFunk($ele, x);
//
// 						$(this).stop().transition({
// 							y : -90,
// 							delay : (i*100)
// 						}, 500, 'ease');
// 					});
//
// 				} else {
//
// 					$ele.find('.content-sgl').each(function(i){
// 						$(this).stop().transition({
// 							y : 20,
// 							delay : (i*100)
// 						}, 500, 'ease', function(){
//
// 							switchFunk($ele, x);
//
// 							$(this).stop().delay(1000).transition({
// 								y : -90,
// 								delay : (i*100)
// 							}, 500, 'ease');
// 						});
// 					});
//
// 				}
//
// 			//}
//
// 			//}
//
// 			} else if(direction == 'up'){
//
//
//
// 				if(x=='2') {
//
//
// 				} else {
//
//
//
// 					$ele.find('.content-sgl').each(function(i){
// 						$(this).stop().transition({
// 							y : 20,
// 							delay : (i*100)
// 						}, 500, 'ease', function(){
//
// 							switchFunk($ele, y);
//
// 							$(this).stop().delay(1000).transition({
// 								y : -90,
// 								delay : (i*100)
// 							}, 500, 'ease');
// 						});
// 					});
//
// 				}
//
// 			}
//
// 		}
//
// 	});
//
// 	$('.article').waypoint(function(direction) {
//
// 		var $this = $(this);
// 		var $ele = $("#associated-content");
//
// 		var x = $this.attr('data-art');
//
// 		if(direction == 'down') {
//
// 			$('#nav > .chapters').find('li').eq(x).addClass('reached');
// 			$('#nav > .chapters').find('li').eq(x).prev().removeClass('reached');
//
// 		} else if(direction == 'up'){
//
// 			$('#nav > .chapters').find('li').eq(x).removeClass('reached');
// 			$('#nav > .chapters').find('li').eq(x).prev().addClass('reached');
//
// 		}
//
// 	});
// });
//
// //associated content script
// $(function(){
//
// 	var $ele = $("#associated-content");
//
//
// 	$(window).scroll(function() {
//         if ($('body').height() <= ($(window).height() + $(window).scrollTop())) {
//             //alert('Bottom reached!');
//         }
//     });
//
//
// });












/**all of the scrollmagic scripts**/
var $section_one = $('#section_one_change_block'),
	 $section_two = $('#section_two_change_block'),
    flag = 0;

function addRemoveFlip(flag, section) {

	console.log(flag);

	if(flag == 1) {

		section.removeClass('flip');

	}else {

		section.addClass('flip');

	}

}

// init controller
var controller = new ScrollMagic.Controller();



new ScrollMagic.Scene({
		  triggerElement: '#about',
        duration: 300,    // the scene should last for a scroll distance of 100px
        offset: 150        // start this scene after scrolling for 50px
    })
    .setPin("#quote_1") // pins the element for the the scene's duration
	 .setTween("#quote_1", 1, {opacity: "1"})
    .addTo(controller); // assign the scene to the controller

// create a scene
new ScrollMagic.Scene({
		  triggerElement: '#midvid-cont1',
        duration: 2100,    // the scene should last for a scroll distance of 100px
        offset: 450        // start this scene after scrolling for 50px
    })
    .setPin("#quote_2") // pins the element for the the scene's duration
	 .setTween("#quote_2", 1, {transform: "translateX(0)"})
	 //.setTween(pinani)
    .addTo(controller); // assign the scene to the controller


new ScrollMagic.Scene({
		  triggerElement: '#midvid-cont2',
        duration: 2100,    // the scene should last for a scroll distance of 100px
        offset: 450        // start this scene after scrolling for 50px
    })
    .setPin("#quote_3") // pins the element for the the scene's duration
	.setTween("#quote_3", 1, {transform: "translateX(0)"})
    .addTo(controller); // assign the scene to the controller




// new ScrollMagic.Scene({
// 		  triggerElement: '#midvid-cont3',
//         duration: 2100,    // the scene should last for a scroll distance of 100px
//         offset: 450        // start this scene after scrolling for 50px
//     })
//     .setPin("#quote_4") // pins the element for the the scene's duration
//     .addTo(controller); // assign the scene to the controller


// var pinani = new TimelineMax()
//     .add(TweenMax.to("#quote_2", 1, {transform: "translateX(-100)"}));


	var img = $('#section_two_change_block').find('img'),
	 	 img_2 = $('#section_three_change_block').find('img'); // Get my img elem

	var blockheight1,
		 blockheight3,
		 copyheight1,
		 copyheight2,
		 copyheight3,
		 copyheight4,
		 copyheightwrapper,
		 offsetCalc,
		 offsetCalc3,
		 viewport_height = $(window).height()/2,
		 duration_scn_1,
		 duration_scn_2,
		 duration_scn_3,
		 duration_scn_4;


$("<img/>") // Make in memory copy of image to avoid css issues

    .attr("src", $(img).attr("src"))
    .load(function() {
		blockheight1 = this.height;   // Note: $(this).width() will not

		offsetCalc = blockheight1/3.5;

		copyheight1 = $('#article_1').outerHeight();
		copyheight2 = $('#article_2').outerHeight();

		duration_scn_1 = (copyheight1 - blockheight1 + viewport_height);
		duration_scn_2 = (copyheight2 - blockheight1 + viewport_height);

		startScroll();

});


$("<img/>") // Make in memory copy of image to avoid css issues

    .attr("src", $(img_2).attr("src"))
    .load(function() {

		blockheight3 = this.height;   // Note: $(this).width() will not

		copyheight3 = $('#article_3').outerHeight();
		copyheight4 = $('#article_4').outerHeight();
		copyheightwrapper = $('#article_3_4_wrapper').outerHeight();

		//offsetCalc3 = blockheight3/3.5;

		duration_scn_3 = (copyheight3 - blockheight3 + viewport_height);
		duration_scn_4 = (copyheight4 - blockheight3 + viewport_height);

		//console.log('>>>>' + duration_scn_3, viewport_height, offsetCalc3);

		startScroll_2();

});





function startScroll(){

//section one switching images on scroll
new ScrollMagic.Scene({
		  triggerElement: '#article_1',
        duration: duration_scn_1,    // the scene should last for a scroll distance of 100px
        offset: offsetCalc        // start this scene after scrolling for 50px
    })
    .setPin("#section_one_change_block") // pins the element for the the scene's duration
	.on("start", function (e) {

		if(e.target.controller().info("scrollDirection") === 'FORWARD') {
			$('#section_one_change_block').velocity({
				opacity : 1
			}, 350, 'easeOutSine');
		} else {
			$('#section_one_change_block').velocity({
				opacity : 0
			}, 350, 'easeOutSine', function(){
				$section_one.find('.front > img').attr('src', '../img/tidus.png');
				flag = 1;
				addRemoveFlip(flag, $section_one);
			});
		}

	 })
	.on("progress", function (e) {

		console.log(e.progress.toFixed(2));

			if(e.progress.toFixed(2) >= 0 && e.progress.toFixed(2) <= 0.05) {

				flag = 1;
				$section_one.find('.front > img').attr('src', '../img/tidus.png');
				addRemoveFlip(flag, $section_one);

				console.log('switch 1');

			} else if(e.progress.toFixed(2) >= 0.25 && e.progress.toFixed(2) <= 0.3) {

				flag = 0;
				$section_one.find('.back > img').attr('src', '../img/quote_1.png');
				addRemoveFlip(flag, $section_one);

				console.log('switch 1');

			} else if(e.progress.toFixed(2) >= 0.5 && e.progress.toFixed(2) <= 0.55) {

				flag = 1;
				$section_one.find('.front > img').attr('src', '../img/fallout_boy.png');
				addRemoveFlip(flag, $section_one);

				console.log('switch 2');

			} else if(e.progress.toFixed(2) >= 0.75 && e.progress.toFixed(2) <= 0.8) {

				flag = 0;
				$section_one.find('.back > img').attr('src', '../img/lightening_returns.png');
				addRemoveFlip(flag, $section_one);

				console.log('switch 3');

			}

		})
    .addTo(controller); // assign the scene to the controller



//section two switching images on scroll
new ScrollMagic.Scene({
		  triggerElement: '#article_2',
        duration: duration_scn_2,    // the scene should last for a scroll distance of 100px
        offset: offsetCalc        // start this scene after scrolling for 50px
    })
    .setPin("#section_two_change_block") // pins the element for the the scene's duration
	.on("start", function (e) {

		if(e.target.controller().info("scrollDirection") === 'FORWARD') {
			$section_two.velocity({
				opacity : 1
			}, 200);
		} else {
			$section_two.velocity({
				opacity : 0
			}, 200, function(){
				$section_two.find('.front > img').attr('src', '../img/dragon_quest-compressor.png');
				flag = 1;
				addRemoveFlip(flag, $section_two);
			});
		}

	 })
	.on("progress", function (e) {

		console.log(e.progress.toFixed(2));

			if(e.progress.toFixed(2) >= 0 && e.progress.toFixed(2) <= 0.05) {

				flag = 1;
				$section_two.find('.front > img').attr('src', '../img/dragon_quest-compressor.png');
				addRemoveFlip(flag, $section_two);

				console.log('switch 1');

			} else if(e.progress.toFixed(2) >= 0.25 && e.progress.toFixed(2) <= 0.3) {

				flag = 0;
				$section_two.find('.back > img').attr('src', '../img/ultima.png');
				addRemoveFlip(flag, $section_two);

				console.log('switch 1');

			} else if(e.progress.toFixed(2) >= 0.5 && e.progress.toFixed(2) <= 0.55) {

				flag = 1;
				$section_two.find('.front > img').attr('src', '../img/quote_2.png');
				addRemoveFlip(flag, $section_two);

				console.log('switch 2');

			} else if(e.progress.toFixed(2) >= 0.75 && e.progress.toFixed(2) <= 0.8) {

				flag = 0;
				$section_two.find('.back > img').attr('src', '../img/ffiv.png');
				addRemoveFlip(flag, $section_two);

				console.log('switch 3');

			}

		})
    .addTo(controller); // assign the scene to the controller

};


function startScroll_2(){

		new ScrollMagic.Scene({
			  triggerElement: '#article_3',
		     duration: duration_scn_3,    // the scene should last for a scroll distance of 100px
			  pushFollowers: false,
		     offset: viewport_height        // start this scene after scrolling for 50px
		 })
		 .setPin("#section_three_change_block") // pins the element for the the scene's duration
		 .setTween(wipeAnimation)
		 .on("start", function (e) {})
		.on("progress", function (e) {})
		.addTo(controller); // assign the scene to the controller


		new ScrollMagic.Scene({
			  triggerElement: '#article_4',
		     duration: duration_scn_4,    // the scene should last for a scroll distance of 100px
			  pushFollowers: false,
		     offset: viewport_height        // start this scene after scrolling for 50px
		 })
		 .setPin("#section_four_change_block") // pins the element for the the scene's duration
		 .setTween(wipeAnimation_2)
		 .on("start", function (e) {})
		 .on("progress", function (e) {})
		 .addTo(controller); // assign the scene to the controller


		// new ScrollMagic.Scene({
		// 	  triggerElement: '#article_3_4_wrapper',
		//      duration: copyheightwrapper,    // the scene should last for a scroll distance of 100px
		// 	  pushFollowers: false,
		//      offset: viewport_height        // start this scene after scrolling for 50px
		//  })
		//  .setTween("#article_3_4_wrapper", 1, {backgroundColor: "MintCream", opacity: "1"})
		//  .on("start", function (e) {})
		//  .on("progress", function (e) {})
		//  .addTo(controller); // assign the scene to the controller


};


var wipeAnimation = new TimelineMax()
			.fromTo("section.panel_wipe.two_wipe", 1, {x: "-100%"}, {x: "0%", ease: Linear.easeNone})  // in from left
			.fromTo("section.panel_wipe.three_wipe", 1, {x:  "100%"}, {x: "0%", ease: Linear.easeNone})  // in from right
			.fromTo("section.panel_wipe.four_wipe", 1, {y: "-100%"}, {y: "0%", ease: Linear.easeNone}); // in from top

var wipeAnimation_2 = new TimelineMax()
			.fromTo("section.panel_wipe_2.two_wipe", 1, {x: "-100%"}, {x: "0%", ease: Linear.easeNone})  // in from left
			.fromTo("section.panel_wipe_2.three_wipe", 1, {x:  "100%"}, {x: "0%", ease: Linear.easeNone})  // in from right
			.fromTo("section.panel_wipe_2.four_wipe", 1, {x: "-100%"}, {x: "0%", ease: Linear.easeNone})  // in from left
			.fromTo("section.panel_wipe_2.five_wipe", 1, {y: "-100%"}, {y: "0%", ease: Linear.easeNone}); // in from top
