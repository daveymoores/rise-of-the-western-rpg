module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    sass: {
      options: {
        includePaths: ['bower_components/foundation/scss']
      },
      dist: {
        options: {
          outputStyle: 'compressed',
          sourceMap: true,
        },
        files: {
          'css/app.css': 'scss/app.scss'
        }
      }
    },

    watch: {
      grunt: {
        options: {
          reload: true
        },
        files: ['Gruntfile.js']
      },

      sass: {
        files: 'scss/**/*.scss',
        tasks: ['sass']
      }
   },
    imagemin: {
        png: {
           options: {
            optimizationLevel: 7
           },
           files: [
            {
               // Set to true to enable the following options…
               expand: true,
               // cwd is 'current working directory'
               cwd: 'project-directory/img/',
               src: ['**/*.png'],
               // Could also match cwd line above. i.e. project-directory/img/
               dest: 'project-directory/img/compressed/',
               ext: '.png'
            }
           ]
        },
        jshint: {
          all: ['Gruntfile.js', 'js/*.js']
        },
        jpg: {
           options: {
            progressive: true
           },
           files: [
            {
               // Set to true to enable the following options…
               expand: true,
               // cwd is 'current working directory'
               cwd: 'project-directory/img/',
               src: ['**/*.jpg'],
               // Could also match cwd. i.e. project-directory/img/
               dest: 'project-directory/img/compressed/',
               ext: '.jpg'
            }
           ]
        }
      }
  });

  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.loadNpmTasks('grunt-contrib-jshint');

  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.registerTask('imagemin', ['imagemin']);

  grunt.registerTask('build', ['sass']);
  grunt.registerTask('default', ['build','watch']);
}
